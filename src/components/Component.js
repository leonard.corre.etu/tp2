export default class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.children = children;
		this.attribute = attribute;
	}

	render() {
		if (this.children && this.attribute)
			return `<${this.tagName} ${this.attribute.name}=${
				this.attribute.value
			} >${this.renderChildren()}</${this.tagName}>`;
		else if (this.children)
			return `<${this.tagName}>${this.renderChildren()}</${this.tagName}>`;
		else return this.renderAttribute();
	}
	renderAttribute() {
		return `<${this.tagName}  ${this.attribute.name}=${this.attribute.value}/>`;
	}
	renderChildren() {
		let res = '';
		if (this.children instanceof Array)
			this.children.forEach(element => {
				if (element instanceof Component) res += element.render();
				else res += element;
			});
		else res = this.children;
		return res;
	}
}

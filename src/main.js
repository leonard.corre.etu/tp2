import Component from './components/Component.js';
import Img from './components/Img.js';
import PizzaThumbnail from './components/PizzaThumbnail.js';
import data from './data.js';
import PizzaList from './pages/PizzaList.js';

const title = new Component('h1', null, ['La', ' ', 'carte']);
document.querySelector('.pageTitle').innerHTML = title.render();
const pizzaList = new PizzaList(data);
document.querySelector('.pageContent').innerHTML = pizzaList.render();

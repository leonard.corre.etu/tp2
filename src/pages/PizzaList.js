import Component from '../components/Component.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';
import data from '../data';
export default class PizzaList extends Component {
	constructor(data) {
		super('section', { name: 'class', value: 'pizzaThumbnail' }, data);
	}

	render() {
		let res = this.renderAttribute();
		this.children.forEach(element => {
			res += new PizzaThumbnail(element).render();
		});
		return res;
	}
}
